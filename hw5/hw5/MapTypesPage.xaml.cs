﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace hw5
{
    public partial class MapTypesPage : ContentPage
    {
        public MapTypesPage()
        {
            InitializeComponent();
            Title = "CS481 HW5 Maps";

            Picker.Items.Add("Disneyland Park");//add picker into the list.
            Picker.Items.Add("Golden Gate Bridge");
            Picker.Items.Add("LAX Airport");
            Picker.Items.Add("SAN Airport");
        }

        private async void OnButtonClicked(object sender, EventArgs e)
        {
            Button button = sender as Button;
            switch (button.Text)
            {
                case "Street":
                    map.MapType = MapType.Street;
                    break;
                case "Satellite":
                    map.MapType = MapType.Satellite;
                    break;
            }
        }

        //source code original from Jacob cs481, I did some twists on it.
        private void Picker_Choice(object sender, System.EventArgs e)
        {
            string name = Picker.Items[Picker.SelectedIndex];//Prevent ad hoc method
            if (name == "Disneyland Park")//User picks the location
            {
                map.MoveToRegion(MapSpan.FromCenterAndRadius(
                    new Position(33.8120918, -117.9189742), Distance.FromMiles(1)));
            }

            if (name == "Golden Gate Bridge")
            {
                map.MoveToRegion(MapSpan.FromCenterAndRadius(
                    new Position(37.8199328, -122.4804491), Distance.FromMiles(1)));
            }

            if (name == "LAX Airport")
            {
                map.MoveToRegion(MapSpan.FromCenterAndRadius(
                    new Position(33.9415933, -118.410724), Distance.FromMiles(1)));
            }

            if (name == "SAN Airport")
            {
                map.MoveToRegion(MapSpan.FromCenterAndRadius(
                    new Position(32.7338051, -117.1954978), Distance.FromMiles(1)));
            }
        }
    }
}